#!/bin/sh

# add specific configuration options
cat /app/templates/imapd-primary.conf >> /app/templates/imapd.conf
rm -f /app/templates/imapd-primary.conf

sed -i '/START {/a\ \ syncclient    cmd="sync_client -r"' /etc/cyrus.conf

# run base entrypoint
/app/entrypoint.sh
